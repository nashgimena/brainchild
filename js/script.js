$('.menu-xs-btn').click(function(){
	$('.menu-xs').toggleClass('open');
	$('.menu-xs-btn').toggleClass('btn-active');
	if ($('.menu-xs').hasClass('open')) {
		$('.main').addClass('header-content-space');
	} else {
		$('.main').removeClass('header-content-space');
	}
});

$('.menu-xs-links').click(function(){
	$('.menu-xs').removeClass('open');
	$('.content').removeClass('header-content-space');
});


$('.menu li a').mouseover(function(){
	$(this).parent().find('div').addClass('circle-highlight');
});

$('.menu li').mouseout(function(){
	$(this).parent().find('div').removeClass('circle-highlight');
});

$('.menu li').click(function(){
	// $(this).find('div').addClass('circle-highlight-active');
});

$('.menu-md li, .menu-xs li').hover(function(){
	$(this).find('a').toggleClass('active');
	$(this).css('cursor', 'pointer');
});