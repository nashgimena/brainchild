var mainApp = angular.module('mainApp', ['ngRoute']);

mainApp.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : 'pages/aboutus.html',
			controller  : 'aboutusController'
		})

		.when('/aboutus', {
			templateUrl : 'pages/aboutus.html',
			controller  : 'aboutusController'
		})

		.when('/equipment', {
			templateUrl : 'pages/equipment.html',
			controller  : 'equipmentController'
		})

		.when('/product', {
			templateUrl : 'pages/product.html',
			controller  : 'productController'
		})

		.when('/clients', {
			templateUrl : 'pages/clients.html',
			controller  : 'clientsController'
		})

		.when('/contact', {
			templateUrl : 'pages/contact.html',
			controller  : 'contactController'
		})

		.when('/email', {
			templateUrl : 'pages/email.php',
			controller  : 'contactController'
		})

		.otherwise({
			redirectTo  : '/',
			templateUrl : 'pages/aboutus.html',
			controller  : 'aboutusController'
		});
});


mainApp.controller('mainController', function($scope) {

});

mainApp.controller('aboutusController', function($scope) {
	$scope.title = 'COMPANY OVERVIEW';
	$scope.menu = 1;

});

mainApp.controller('equipmentController', function($scope) {
	$scope.title = 'MACHINERY & EQUIPMENT';
	$scope.menu = 2;

});

mainApp.controller('productController', function($scope) {
	$scope.title = 'PRODUCT LINE';
	$scope.menu = 3;

});

mainApp.controller('clientsController', function($scope) {
	$scope.title = 'PARTIAL LIST OF CLIENTS';
	$scope.menu = 4;

});

mainApp.controller('contactController', function($scope) {
	$scope.title = 'CONTACT INFORMATION';
	$scope.menu = 5;

	$scope.initialize = function() {
		var myLatLng = {
			lat: 14.6415723,
			lng: 120.9726744
		};
		
		var map = new google.maps.Map(document.getElementById('map_div'), {
			center: myLatLng,
			zoom: 18
		});
		var marker = new google.maps.Marker({
		  position: myLatLng,
		  map: map,
		  title: 'Hello World!'
		});
	};    
   
	google.maps.event.addDomListener(window, 'load', $scope.initialize);

});

